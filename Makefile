UNAME := $(shell sh -c 'uname')
COMMIT := $(shell sh -c 'git rev-parse HEAD')
BRANCH := $(shell sh -c 'git rev-parse --abbrev-ref HEAD')

ifdef GOBIN
PATH := $(GOBIN):$(PATH)
else
PATH := $(subst :,/bin:,$(GOPATH))/bin:$(PATH)
endif

default: build

installdeps:
	dep ensure

updatedeps:
	dep ensure -update

initdeps:
	dep init

# -gcflags '-N -l' for debug
# -ldflags -w for prod

linux:
	GOOS=linux GOARCH=amd64 make

mac:
	GOOS=darwin GOARCH=amd64 make

build-linux:
	GOOS=linux GOARCH=amd64 make build

build: gen

debug:
	DEBUG_FLAGS="-gcflags '-N -l'" make build

LDFLAGS="-X main.version=$(VERSION) -X main.commit=$(COMMIT) -X main.branch=$(BRANCH) -X main.buildos=$(UNAME)"

gen: fmt lint
	go build -o db-metrics-gen -ldflags ${LDFLAGS} ${DEBUG_FLAGS} db_metrics_gen.go

PKGS=$(shell go list ./... | grep -v vendor)

lint:
	@golint $(PKGS) | grep -v ffjson.go | grep -v "should have comment" | grep -v "underscore in package name" | grep -v "comment on exported" | tee

testall: test vet race cov bench

test:
	@go test ${PKGS}

# Run "short" unit tests
test-short:
	@go test -short ${PKGS}

vet:
	@go vet ${PKGS}

race:
	@go test -race ${PKGS}

cov:
	@rm -f coverage-all.out
	@echo "mode: cover" > coverage-all.out
	$(foreach pkg,$(PKGS),\
		go test -coverprofile=coverage.out -cover -covermode=count $(pkg);\
		tail -n +2 coverage.out >> coverage-all.out;)

cov-ui: cov
	@go tool cover -html=coverage-all.out

bench:
	@go test -bench . $(PKGS)

SRC_CODE=$(shell find . -type f -name "*.go" -not -path "./vendor/*")

fmt:
	@gofmt -l -w ${SRC_CODE}
	@goimports -l -w ${SRC_CODE}

all: installdeps test build
