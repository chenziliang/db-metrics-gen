package gen

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/lib/pq"
	"github.com/pkg/errors"
)

type Config struct {
	Concurrency int
	TotalEvents int
	BatchSize   int

	DBType     string
	DBUser     string
	DBPassword string
	DBHost     string
	DBPort     int
	DBName     string
}

func (c *Config) Conn() string {
	// conn := "postgres://root:@54.183.114.100:26257/metrics?sslmode=disable"
	if c.DBType == "postgres" {
		return fmt.Sprintf("%s://%s:%s@%s:%d/%s?sslmode=disable", c.DBType, c.DBUser, c.DBPassword, c.DBHost, c.DBPort, c.DBName)
	}

	if len(c.DBPassword) > 0 {
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", c.DBUser, c.DBPassword, c.DBHost, c.DBPort, c.DBName)
	}
	return fmt.Sprintf("%s@tcp(%s:%d)/%s", c.DBUser, c.DBHost, c.DBPort, c.DBName)
}

type ConcurrentMetricGen struct {
	config *Config
}

func NewConcurrentMetricGen(config *Config) (*ConcurrentMetricGen, error) {
	return &ConcurrentMetricGen{
		config: config,
	}, nil
}

func (cg *ConcurrentMetricGen) Gen() error {
	var wg sync.WaitGroup

	share := cg.config.TotalEvents / cg.config.Concurrency
	for i := 0; i < cg.config.Concurrency-1; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()
			if err := cg.gen(id, share); err != nil {
				log.Printf("Worker id=%d failed to gen %d events, error=%s", id, share, err)
			}
		}(i + 1)
	}

	lastShare := cg.config.TotalEvents/cg.config.Concurrency + cg.config.TotalEvents%cg.config.Concurrency
	cg.gen(cg.config.Concurrency, lastShare)
	wg.Wait()

	return nil
}

func (cg *ConcurrentMetricGen) gen(id int, totalEvents int) error {
	log.Printf("Gen worker id=%d starts with share=%d", id, totalEvents)

	mg, err := NewMetricGen(cg.config.DBType, cg.config.Conn())
	if err != nil {
		return err
	}
	defer mg.Close()

	left := totalEvents
	for left > 0 {
		batchSize := cg.config.BatchSize
		if left < batchSize {
			batchSize = left
		}

		if err := mg.BatchLoad(batchSize); err != nil {
			return err
		}
		left -= batchSize
	}

	log.Printf("Gen worker id=%d finished share=%d", id, totalEvents)
	return nil
}

func (cg *ConcurrentMetricGen) Query() (int, error) {
	db, err := sql.Open(cg.config.DBType, cg.config.Conn())
	if err != nil {
		return 0, errors.Wrapf(err, "failed to open")
	}
	defer db.Close()

	// rows, err := db.Query(fmt.Sprintf(`SELECT %s, %s, %s, %s, %s FROM %s`, colRegion, colInstanceID, colOS, colUtil, colTimestamp, table))
	var div string
	if cg.config.DBType == "postgres" {
		div = "/"
	} else {
		div = "DIV"
	}

	roundToMin := fmt.Sprintf(`SELECT %s, %s, %s, %s %s 6000000000 AS timestamp FROM %s`,
		colRegion, colInstanceID, colUtil, colTimestamp, div, table)
	query := fmt.Sprintf(`SELECT %s, %s, %s, AVG(%s) as %s FROM (%s) AS t GROUP BY %s, %s, %s`,
		colRegion, colInstanceID, colTimestamp, colUtil, colUtil, roundToMin, colRegion, colInstanceID, colTimestamp)
	rows, err := db.Query(query)
	if err != nil {
		return 0, errors.Wrap(err, "failed to open query")
	}
	defer rows.Close()

	n := 0
	for rows.Next() {
		n++
		var region, instanceID string
		var util float64
		var timestamp int64

		if err := rows.Scan(&region, &instanceID, &util, &timestamp); err != nil {
			return 0, errors.Wrap(err, "failed to scan")
		}

		fmt.Printf("%s %s %f %d\n", region, instanceID, util, timestamp)
	}

	return n, nil
}

// schema
const (
	table         = "cpuutils"
	colRegion     = "region"
	colInstanceID = "instanceid"
	colOS         = "os"
	colUtil       = "util"
	colTimestamp  = "timestamp"
)

type MetricGen struct {
	dbType string
	db     *sql.DB
}

func NewMetricGen(dbType string, conn string) (*MetricGen, error) {
	db, err := sql.Open(dbType, conn)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open db=%s", dbType)
	}

	return &MetricGen{
		dbType: dbType,
		db:     db,
	}, nil
}

func (g *MetricGen) prepareStatement() string {
	if g.dbType == "postgres" {
		return pq.CopyIn(table, colRegion, colInstanceID, colOS, colUtil, colTimestamp)
	}
	return fmt.Sprintf("INSERT INTO %s VALUES(? ,? ,? ,? ,?)", table)
}

func (g *MetricGen) BatchLoad(batchSize int) error {
	txn, err := g.db.Begin()
	if err != nil {
		return errors.Wrap(err, "failed to start txn")
	}

	stmt, err := txn.Prepare(g.prepareStatement())
	if err != nil {
		return errors.Wrap(err, "failed to prepare txn")
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	for i := 0; i < batchSize; i++ {
		region := fmt.Sprintf("us-west-%d", 1+r.Intn(12))
		instanceID := fmt.Sprintf("instance-%d", 1+r.Intn(10000))
		if _, err := stmt.Exec(region, instanceID, "Linux", r.Float64()*100, time.Now().UnixNano()); err != nil {
			return errors.Wrap(err, "failed to Exec")
		}
	}

	/*if _, err := stmt.Exec(); err != nil {
		return errors.Wrap(err, "failed to Batch Exec")
	}*/

	if err := stmt.Close(); err != nil {
		return errors.Wrap(err, "failed to Close")
	}

	if err := txn.Commit(); err != nil {
		return errors.Wrap(err, "failed to Commit")
	}

	return nil
}

func (g *MetricGen) Close() error {
	return g.db.Close()
}
