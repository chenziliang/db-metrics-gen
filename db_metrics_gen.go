package main

import (
	"log"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"

	"gitlab.com/chenziliang/db-metrics-gen/gen"
)

func cliParams() (string, *gen.Config) {
	dbType := kingpin.Flag("db-type", "[postgres|mysql]").Required().Enum("postgres", "mysql")
	dbUser := kingpin.Flag("db-user", "db user").Required().String()
	dbPassword := kingpin.Flag("db-password", "db password").String()
	dbHost := kingpin.Flag("db-host", "db host/ip").Required().String()
	dbPort := kingpin.Flag("db-port", "db port").Required().Int()
	dbDatabase := kingpin.Flag("db-name", "database name").Required().String()
	// dbSSLMode := kingpin.Arg("ssl-mode", "db SSL").Default("disable").String()

	genCmd := kingpin.Command("gen", "gen events")
	concurrency := genCmd.Flag("concurrency", "number of goroutines").Default("10").Int()
	batchSize := genCmd.Flag("batch-size", "number of events in one transaction").Default("1000").Int()
	totalEvents := genCmd.Flag("total-events", "total number of events to generate").Default("1000000").Int()

	kingpin.Command("query", "query events")

	cmd := kingpin.Parse()

	return cmd, &gen.Config{
		Concurrency: *concurrency,
		BatchSize:   *batchSize,
		TotalEvents: *totalEvents,

		DBType:     *dbType,
		DBHost:     *dbHost,
		DBPort:     *dbPort,
		DBUser:     *dbUser,
		DBPassword: *dbPassword,
		DBName:     *dbDatabase,
	}
}

func main() {
	cmd, config := cliParams()
	g, err := gen.NewConcurrentMetricGen(config)
	if err != nil {
		log.Fatalf("Failed to NewConccurentMetricGen, err=%s", err)
	}

	start := time.Now().Unix()
	defer func() {
		took := time.Now().Unix() - start
		if took == 0 {
			took++
		}

		if cmd == "query" {
			log.Printf("query %d events took %d seconds, eps=%d",
				config.TotalEvents, took, config.TotalEvents/int(took))
		} else {
			log.Printf("generate %d events took %d seconds, eps=%d, batch=%d, concurrency=%d",
				config.TotalEvents, took, config.TotalEvents/int(took), config.BatchSize, config.Concurrency)
		}
	}()

	if cmd == "query" {
		n, err := g.Query()
		if err != nil {
			log.Fatalf("Failed to query, err=%s", err)
		}
		config.TotalEvents = n
	} else {
		if err := g.Gen(); err != nil {
			log.Fatalf("Failed to gen, err=%s", err)
		}
	}
}
